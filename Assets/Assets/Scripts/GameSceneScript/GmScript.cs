﻿using UnityEngine;

/// <summary>
/// Game Manager 줄여서 Gm의 Script.
/// 사실 상, 오브젝트 현황을 관리한다.
/// Public 함수 : GetselectedIndex // GetCountObject // SetCountObject
public class GmScript : MonoBehaviour
{
    public static GmScript gameManagerScript;

    
    private int countObject = 0; //현재 선택 된
    private int selectedIndex = 10; ////최대 선택 가능한


    void Awake(){ Application.targetFrameRate = 60; }

    private void Start() {
        gameManagerScript = this.GetComponent<GmScript>();
    }

    /// <summary>
    /// 오브젝트를 선택 할 수 있는 최대 개수를 반환한다.
    /// </summary>
    public int GetselectedIndex() { return selectedIndex; }

    /// <summary>
    /// 현재 선택 된 오브젝트의 개수를 반환한다.
    /// </summary>
    public int GetCountObject() { return countObject; }

    /// <summary>
    /// 주로 초기화에 사용 될 예정이다.
    /// 현재 선택 된 오브젝트의 개수를 설정한다.
    /// </summary>
    /// <param name="coNum"></param>
    public void SetCountObject(int coNum)
    {
        countObject = coNum;
        Debug.Log("CountObject :: " + GetCountObject() + " ::");
    }




    public void CatchNullExeption(GameObject targetObject)
    {
        _CatchNullExeption(targetObject, targetObject);
    }
    public void CatchNullExeption(GameObject targetObject, GameObject findObject)
    {
        _CatchNullExeption(targetObject, findObject);
    }
    /// <summary>
    /// Checking that if object is not exist.
    /// If object's state is null, Find object by Inputed ID.
    /// </summary>
    /// <param name="targetObject">Plz Input Obj name by ▷real name◁ </param>
    private void _CatchNullExeption(GameObject targetObject, GameObject findObject)
    {
        try
        {
            if (targetObject != null)
                Debug.Log(targetObject.name + " is running");
            // Do something that can throw an exception
        }
        catch (System.NullReferenceException Error)
        {
            //Push ErrLog and Find Object
            Debug.Log("ERR From " + targetObject.name + " :: " + Error);
//            Debug.LogException(Error, this);

            targetObject = GameObject.Find(targetObject.name);
        }
    }
}
