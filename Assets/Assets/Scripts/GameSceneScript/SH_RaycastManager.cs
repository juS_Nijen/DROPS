﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 이름을 바꿀 예정입니다. 기능은 아래와 같습니다.
/// RayCastHit(), RayCastMouse(), UpMouse(), InternallyCheck(), HowManyPang(int repeatNum)
/// </summary>
public class SH_RaycastManager : MonoBehaviour {
    public List<ObjectStatus> selectedList = new List<ObjectStatus>();
    public ObjectStatus.Shape firstShape;
    public float detectDistance = 0.75f;

    private int findCnt = 0;

    private GameStates GameStates;
    private ObjectControl ObjectControl;
    private PrefabSpwan spawner;

    void Start()
    {
        GameStates = GameObject.Find("GameManager").GetComponent<GameStates>();
        ObjectControl = GameObject.Find("GameManager").GetComponent<ObjectControl>();
        spawner = GameObject.Find("Spawner").GetComponent<PrefabSpwan>();
    }

    void Update()
    {
        RayCastMouse();
        UpMouse();
    }


    RaycastHit2D RayCastHit()
    {
        RaycastHit2D hit = Physics2D.Raycast (new Vector2
            (Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y),
            Vector2.zero
            );
        return hit;
    }
    

    void RayCastMouse()
    {
        if (Input.GetMouseButton(0))
        {
//            GameTouchEvent(1);

            bool isFirst = (selectedList.Count == 0) ? true : false;

            RaycastHit2D hit = RayCastHit();

            if (hit) //오브젝트를 찾은경우
            {
                //레이케스트로 찾은 오브젝트
                ObjectStatus selected = hit.collider.GetComponent<ObjectStatus>();
                if (isFirst)
                {
                    if (selected.IsSelect == false)
                    {
                        firstShape = selected.ShapeOption;
                        selected.IsSelect = true;
                        selectedList.Add(selected);
                        GmScript.gameManagerScript.SetCountObject(selectedList.Count);
                    }
                }
                else 
                {
                    if (selected.IsSelect == false)
                    {
                        float selectDistance = Vector2.Distance(selectedList[selectedList.Count - 1]
                            .transform.position, selected.transform.position);
                        //만약에 드래그 하려는 오브젝트가 너무 멀리있으면 드래그하지 못하게한다.
                        //++모양이 다른경우 드래그 못하게한다.
                        if ((selectDistance < detectDistance 
                            && selectDistance > -detectDistance) 
                            && selectedList[0].ShapeOption == selected.ShapeOption)
                        {
                            selected.IsSelect = true;
                            selectedList.Add(selected);
                            GmScript.gameManagerScript.SetCountObject(selectedList.Count);
                        }
                    }
                }
            }
        }
    }

    //터치를 구분한다.
    void GameTouchEvent(int checkNumber)
    {
        if (checkNumber == 1)
        {
            GameStates.GameS_Pause();
            GameStates.setCheckNum(1);
        }
        else if (checkNumber == 0)
        {
            GameStates.GameS_Resume();
            GameStates.setCheckNum(2);
        }
    }


    /// <summary>
    /// 마우스를 떼었을 경우
    /// </summary>
    void UpMouse()
    {
        //만약 버튼이 눌리지 않았을 경우, 리턴 해 준다.
        if (!Input.GetMouseButtonUp(0)) return;

        //손을 떼었을 경우 일시정지를 해제.
        InternallyCheck();

        //오브젝트의 수 만큼 반복한다.
        for (int re = 0; re < selectedList.Count; re++)
        {
            HowManyPang(re);
        }

        //오브젝트가 파괴 될 경우 실행
        if (selectedList.Count >= 3)
        {
            spawner.PrefabObject(selectedList.Count);
        }
        //선택 된 리스트를 지운다.
        selectedList.Clear();
        findCnt = 0;
    }

    /// <summary>
    /// 여기서 selectedList에 들어있는 애들을 기준으로 IsSelect가 true인애들이 근처에있으면 묶어서 터트려주는 작업하면댐
    /// </summary>
    void InternallyCheck()
    {
        for (int re = 0; re < selectedList.Count - 1; re++)
        {
            if (re == 0) findCnt++;
            float distance = Vector2.Distance(
                selectedList[re].transform.position,
                selectedList[re + 1].transform.position
            );
            
            // 오브젝트A와 오브젝트B 사이의 거리
            if (distance < detectDistance && distance > -detectDistance)
            //-1 ~ 1 사이에있는 거리라면 그 아이들은 찾았다고 판단한다.
            {
                if (selectedList[re + 1].GetComponent<ObjectStatus>().ShapeOption
                !=
                selectedList[0].GetComponent<ObjectStatus>().ShapeOption) //맨처음 선택한 오브젝트랑 모양이 다르다면
                {
                    findCnt = 0;
                    return;
                }
                findCnt++;
            }
        }
    }

    void HowManyPang(int repeatNum)
    {
        if (findCnt >= 3)
        {
            if (selectedList[repeatNum].IsSelect == true)
                Destroy(selectedList[repeatNum].gameObject);
        }
        if ((findCnt >= 3) && ((findCnt < 5)))
        {
            ObjectControl.Pang_Upper3();
        }
        else if (findCnt >= 5)
        {
            ObjectControl.Pang_Upper5();
        }
        selectedList[repeatNum].IsSelect = false;
        selectedList[repeatNum].isAlreadyCheck = false;
        firstShape = 0;
    }
}