﻿using UnityEngine;

/// <summary>
/// 얘네 이름 바꿀거다.
/// 내용물이 좀 구림. 구조화는 해도 ㄱㅊ
/// </summary>
public class ObjectStatus : MonoBehaviour {
    public enum Shape
    {
        Circle = 101,
        Box = 201,
        TriAngle = 301
    }
    public Shape ShapeOption;
    public bool isSelect;
    public bool IsSelect
    {
        get { return isSelect; }
        set
        {
            if(value == true)
            {
                isSelect = value;
                Color_Selected();                
            }
            else
            {
                isSelect = value;
                Color_UnSelected();
            }
        }

    }
    public bool isAlreadyCheck;

    void Color_Selected()
    {
        gameObject.GetComponent<SpriteRenderer>().color
            = new Color(185 / 255f, 185 / 255f, 185 / 255f, 1f);
    }

    void Color_UnSelected()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
