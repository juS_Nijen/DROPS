﻿using UnityEngine;

/// <summary>
/// (구)BoxSpawn. 이름이 변경되었다.
/// Public 함수 : objectType // _PrefabObject // GetObjectType
/// </summary>
public class PrefabSpwan : MonoBehaviour
{
    //Spawner의 위치를 받아옵니다.
    public Transform rot;

    //Prefab를 유동적으로 변환이 가능하도록 도와줍니다.
    public GameObject[] objectType;
    /* 
        엔진에서 받아내기 위하여 Public로 설정하였기 떄문에,
        일단은 권한설정을 Pravate로 바꾸지 말아주세요.
    */


    /// <summary>
    /// Prefab를 생성한다. 떨어지는 동안 무언가를 설정해야할 듯.
    /// </summary>
    /// <param name="_objectType"></param>
    /// <param name="_howManyPrefab"></param>
    public void PrefabObject(GameObject _objectType, int _howManyPrefab)
    {
        GmScript.gameManagerScript.CatchNullExeption(_objectType, null);

        for (int Count = 0; Count < _howManyPrefab; Count++)
        {
            Vector2 pos = new Vector2(Random.Range(-2.0f, 1.75f), Random.Range(1.5f, 4.5f));
            //obj에 찍어낼 형태, 위치, 각도를 넣어주었다.
            var obj = Instantiate(_objectType, pos, rot.rotation);
            obj.name = _objectType.tag + "_" + Count;
        }
    }


    //PrefabObject의 오버로드(랜덤스폰)
    public void PrefabObject(int _howManyPrefab)
    {
        //오브젝트가 존재 할 경우
        for (int Count = 0; Count < _howManyPrefab; Count++)
        {
            int objNum;
            Vector2 pos = new Vector2(Random.Range(-2.0f, 1.75f), Random.Range(1.5f, 4.5f));
            //obj에 찍어낼 형태, 위치, 각도를 넣어주었다.

            objNum = Random.Range(0, 3);
            var obj = Instantiate(objectType[objNum], pos, rot.rotation);

            //이름은 Imsi_box_(번호)로 지정.
            obj.name = objectType[objNum].tag + "_" + Count;
        }
    }
}