using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// PausePopup is script to use, when pause button clicked.
/// </summary>
public class PausePopup : MonoBehaviour {

    public  GameObject pauseGame;
    private GameStates GameState;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        GameState = GameObject.Find("GameManager").GetComponent<GameStates>();
    }


    /// <summary>
    /// PopUpOpen is called to open game pause window.
    /// </summary>
    public void PopUpOpen()
    {
        pauseGame.SetActive(true);
        GameState.SetOnUIPuase(true);
        GameState.GameS_Pause();
    }

    /// <summary>
    /// PopUpClose is called to close game pause window.
    /// </summary>
    public void PopUpClose()
    {
        pauseGame.SetActive(false);
        GameState.SetOnUIPuase(false);
        GameState.GameS_Resume();
    }

    /// <summary>
    /// ReplayOnClick is called to replay game.
    /// </summary>
    public void ReplayOnClick()
    {
        SceneManager.LoadScene("GameScene");
        PopUpClose();
    }

    /*
    Gohome is finish game and go select scence.
    But use GameState.ScenChange() to scence close.
    */

    /// <summary>
    /// HelpOnClick is called to pause game and show help window.
    /// </summary>
    public void HelpOnClick()
    {
        Debug.Log("help button clicked");
    }

    /// <summary>
    /// SettingOnClick is called to pause game and show setting window.
    /// </summary>
    public void SettingOnClick()
    {
        Debug.Log("setting button clicked");
    }
}