﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 타이머 종료 후 결과 창 팝업에 사용 될 스크립트입니다.
/// </summary>
public class ResultPopUp : MonoBehaviour {

    public  GameObject resultPopUp;
    private GameStates GameState;

    private ObjectControl objControlScript;

    void Start ()
    {
        GameState = GameObject.Find("GameManager").GetComponent<GameStates>();
        objControlScript = GameObject.Find("GameManager").GetComponent<ObjectControl>();
    }


    /// <summary>
    /// 팝업창을 엽니다.
    /// </summary>
    public void PopUpOpen()
    {
        GameState.SetOnUIPuase(true);
        GameState.GameS_Pause();
        resultPopUp.SetActive(true);
        SetScore();
    }
    /// <summary>
    /// 팝업창을 닫습니다.
    /// </summary>
    public void PopUpClose()
    {
        GameState.SetOnUIPuase(false);
        GameState.GameS_Resume();
        resultPopUp.SetActive(false);
    }

    public void SetScore()
    {
        Text scoreText = GameObject.Find("ScoreText_Back").GetComponent<Text>();

        string SetText = objControlScript.GetScore().ToString();
        scoreText.text = SetText;
    }
}
