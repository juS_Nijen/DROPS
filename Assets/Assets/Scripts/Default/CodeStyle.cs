﻿//코딩 스타일에 관련된 메모들입니다.
//이곳에 스타일을 적어둡시다.

/* CodeStyle.cs

- 대괄호 : 대괄호는 아래로

- 행, 열 : 열이 80이 넘어가면 아래로 자른다.
           선 관련 된 툴이 있으면 편할 것이다.

- 이름   : 변수명은 소문자로 시작.
           클래스명은 대문자로 시작.
           Object > obj

           스크립트 이름은 대문자 따온 뒤 Script
           PrefabManagerScript > pmScript
           gameObject < 구분명은 대문자로.

- 주석   : 두 줄 이상은 블럭주석을 사용하자.
           함수 주석은 /// << 이용하여 표기.
           클래스가 어떤 역할을 하는지 주석을 달아둡시다.

- 칸     : int, char 등의 빈 칸은 Tab키로 맞춘다.
           나머지는 SpaceBar

- 어떤 함수가 있는 지 정리하고 들어가는 게 좋지 않을까?


규칙

- checkNumber _10 == 정상적인 시간의 흐름
              _01 == Pause  Game Event
              _00 == Resume Game Event, Nothing happend
*/