﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Back 키를 눌러 종료 했을 경우의 이벤트
/// </summary>
public class AndroidBack : MonoBehaviour
{
    private int gold_IMSI = 0;
    private int highRecord = 0;
    private int highRecord_IMSI = 0;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerPrefs.SetInt("User_Gold", gold_IMSI);

            //하이스코어만 저장
            if (highRecord < highRecord_IMSI)
            {
                PlayerPrefs.SetInt("User_HighRecord", highRecord_IMSI);
            }

            PlayerPrefs.Save();
            Application.Quit(); //종료
        }
    }
}
