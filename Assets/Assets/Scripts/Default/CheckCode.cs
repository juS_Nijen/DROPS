﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCode : MonoBehaviour {


    ///체크 값을 저장한다.

    enum checkCode
    {
        Developement = 0,

        Success = 3,
        Error = 4,
    
        ModeDefault = 10, 
        Mode_1 = 20,
        Mode_2 = 30,
        Mode_3 = 40,
        Mode_4 = 50,

    };


    /*
    체크값을 받아와야하지않을까
    모드를 골랐을 때 어떤 모드인가
     
    
    씬 이동 >> 모드 1 모드 2 모드 3
    체크 값 10을 받았을 때 오브젝트를 켜기
    
    게임 매니저에 모드 값을 넣어준다.
    
    
    기본 값 : 10단위로 상승
    
    기본 모드 :: 10

    ~ 그 중간 사이 어딘가 :: 11, 12, 13, 14 ,,,
    움직이지 않는 도형 모드 :: 20 

    씬 이동 할 때 매개변수 전송.


	기본적인 씬의 스테터스 :: 0~9
	1 :: 정상적인 실행
     */
}