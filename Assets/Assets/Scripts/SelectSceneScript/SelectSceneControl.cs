﻿using UnityEngine;

/// <summary>
/// Select Scene의 관리를 위한 클래스.
/// </summary>
public class SelectSceneControl : MonoBehaviour
{
    public static SelectSceneControl selectSceneControl;

    void Start()
    { selectSceneControl = GameObject.Find("GameManager").GetComponent<SelectSceneControl>(); }

    public GameObject popShop;
    public GameObject popOption;
    public GameObject selectText;


    //selectStateMax의 배수로 지정 해 줄 것.
    public int selectState = 30;
    private int selectStateMax = 3;


    public void ArrowLeft()
    {
        selectState--; StateIsNotUnderMax();
        SelectImageManagement.selectImageManagement
            .ImageOnOff(selectState, selectStateMax);
    }
    public void ArrowRight()
    {
        selectState++; StateIsNotUnderMax();
        SelectImageManagement.selectImageManagement
            .ImageOnOff(selectState, selectStateMax);
    }

    private void StateIsNotUnderMax() { if (selectState < 3) selectState *= selectStateMax; }


    /// <summary>
    /// 현재 상태를 체크
    /// </summary>
    public void stateRun()
    {
        // Default = 게임시작, 1 = 유적, 2 = 옵션
        switch (selectState % selectStateMax)
        {
            case 0: GameStart();  break;
            //임시로 업적을 띄워줄 예정이다.
            case 1: ShopOpen();   break;
            case 2: OptionOpen(); break;
        }
    }


    public void GameStart() { GameStates.gameStates.SceneChange("ModeSelect"); }

    //Manage Item Shop
    public void ShopOpen()  { SelectTextOff(); popShop.SetActive(true); }
    public void ShopClose() { SelectTextOn();  popShop.SetActive(false); }
    //Manage Game Option
    public void OptionOpen()  { SelectTextOff(); popOption.SetActive(true); }
    public void OptionClose() { SelectTextOn();  popOption.SetActive(false); }
    //Set SelectText's State whem Pop-up is Active.
    private void SelectTextOn()  { selectText.SetActive(true); }
    private void SelectTextOff() { selectText.SetActive(false); }
}
