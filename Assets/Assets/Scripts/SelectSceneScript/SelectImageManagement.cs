﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectImageManagement : MonoBehaviour
{
    public GameObject[] statusImageObjects = new GameObject[3];

    public static SelectImageManagement selectImageManagement;

    void Start()
    { selectImageManagement = GameObject.Find("GameManager").GetComponent<SelectImageManagement>(); }

    public void ImageOnOff(int index, int indexMax)
    {
        statusImageObjects[index % indexMax].SetActive(true);
        statusImageObjects[(index + 1) % indexMax].SetActive(false);
        statusImageObjects[(index + 2) % indexMax].SetActive(false);
    }
}