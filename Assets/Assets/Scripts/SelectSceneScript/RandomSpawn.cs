﻿using UnityEngine;
using System.Collections;

public class RandomSpawn : MonoBehaviour
{
    //Prefabs that will be spawn ingames
    public  GameObject[] cloudPrefabs = new GameObject[6];

    //It can't be spawn on limited areas
    private Vector3 imageSpawnPosition;
    public  Vector3 posLeft, posRight;
    public  float   posNum = 5.0f;


    private void Start()
    {
        imageSpawnPosition.x = posRight.x;
        StartCoroutine(Prefab());
    }

    //이름 다시 지을 것. 이름은 비워둘 수 없습니다.
    IEnumerator Prefab()
    {
        Instantiate(cloudPrefabs[Random.Range(1, 6)],
            RandomSpawning(), Quaternion.identity);

        yield return new WaitForSeconds(7.5f);

        StartCoroutine(Prefab());
    }

    private Vector3 RandomSpawning()
    {
        if (imageSpawnPosition.x < posNum)
        {
            imageSpawnPosition = new Vector3(posLeft.x,  Random.Range(-2.5f, 5.0f), 0 );
        }
        else if (imageSpawnPosition.x > posNum)
        {
            imageSpawnPosition = new Vector3(posRight.x, Random.Range(-2.5f, 5.0f), 0);
        }
        return imageSpawnPosition;
    }
}
