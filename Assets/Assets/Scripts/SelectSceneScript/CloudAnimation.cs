﻿using UnityEngine;
using System.Collections;

public class CloudAnimation : MonoBehaviour
{
    public float moveSpeed = 3.0f;
    bool rightHand = false;

    private void Start()
    {
        StartCoroutine(CoroutineTimer(60f));
    }
    private void Update()
    {
        if (rightHand) MoveCloudToLeft();
        else if (!rightHand) MoveCloudToRight();
    }
    
    
    private void MoveCloudToLeft() {
        Vector3 moveLeft = new Vector3(moveSpeed * Time.deltaTime, 0f, 0f);
        gameObject.transform.position += moveLeft;
    }
    private void MoveCloudToRight() {
        Vector3 moveRight = new Vector3(moveSpeed * Time.deltaTime, 0f, 0f);
        gameObject.transform.position -= moveRight;
    }

 
    public IEnumerator CoroutineTimer(float sec)
    {
        yield return new WaitForSeconds(sec);
        Destroy(gameObject);
    }
}