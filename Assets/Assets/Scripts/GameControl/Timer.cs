﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// 게임 내에서의 시간에 관련된 기능들이 담겨있다.
/// </summary>
public class Timer : MonoBehaviour
{
    //UI를 위하여 float 형식으로 남겨두자, 남은 시간
    private float leftTime = 30;
    //전체 시간 (UI에 응용)
    private float maxTime  = 30;
    //시간을 보여주는 TimeSlider UI
    private Image timeSlider;


    private void Start()
    {
        leftTime = maxTime;
        timeSlider = GameObject.Find("TimeBar").GetComponent<Image>();

        //코루틴을 시작 해 주는 메서드를 시작.
        TimerCallBack();
    }


    /// <summary>
    /// sec(원하는시간)씩 시간을 감소시켜준다.
    /// </summary>
    /// <returns></returns>
    public IEnumerator CoroutineTimer(float sec, int check)
    {
        //WaitForSeconds객체를 생성해서 반환, sec초씩 감소.
        yield return new WaitForSeconds(sec);
        leftTime -= sec;

        //Yell에서 호출 했을 경우에만 소환.
        if (check == 10)
            TimerCallBack();
    }

    /// <summary>
    /// 게임이 시작되면 카운트를 시작한다.
    /// </summary>
    public void TimerCallBack()
    {
        //UI에 적용 해 준다.
        SetTimeUI();

        //시간이 남아있을 경우, 코루틴을 실행시켜 준다.
        if (leftTime > 1)
            StartCoroutine(CoroutineTimer(0.1f, 10));
    }

    /// <summary>
    /// 타이머의 값을 UI에 보이도록 넣어준다.
    /// </summary>
    public void SetTimeUI()
    {
        timeSlider.fillAmount = leftTime / maxTime;
        if (leftTime < 1) TimeOver();
    }

    /// <summary>
    /// 임의로 시간 감소 (디버프)
    /// </summary>
    void decreaseTimeRemaining(float decreaseTime)
    {
        leftTime -= decreaseTime;
    }

    /// <summary>
    /// 시간이 다 되었을 때의 행동을 정의한다.
    /// </summary>
    public void TimeOver()
    {
        ResultPopUp resultPopUpScript = GameObject.Find("GameManager")
                    .GetComponent<ResultPopUp>();
        GameStates gsScript = GameObject.Find("GameManager")
            .GetComponent<GameStates>();

        resultPopUpScript.PopUpOpen();

        gsScript.GameS_Pause();
        gsScript.데이터저장_임시();
    }

    /// <summary>
    /// Pause키를 누르면 일시정지.
    /// </summary>
    public void PausePoP_Open()
    {
        Time.timeScale = 1;
    }
}