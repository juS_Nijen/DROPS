﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundControl : MonoBehaviour {

    //Read Data
    public Slider audioSlider;
    public int soundVolume = 5;

    public GameObject soundOn;
    public GameObject soundOff;
    public AudioSource SoundPlayer;


    public void SetInfo()
    {
        PlayerPrefs.SetInt("VolumeInfo", soundVolume);
        PlayerPrefs.Save();
    }


    public void SetSoundVolume(int slider)
    {
        soundVolume = slider;
        SoundPlayer.volume = soundVolume/100;
        Debug.Log("Set Sound volue = " + SoundPlayer.volume);
    }


    public void IconChange()
    {
        if (soundVolume == 0)
        {
            soundOn.SetActive(false);
            soundOff.SetActive(true);
        }
        else
        {
            soundOn.SetActive(true);
            soundOff.SetActive(false);
        }
    }

    private void SoundInfoLoad()
    {
        if (PlayerPrefs.HasKey("VolumeInfo"))
        {
            soundVolume = PlayerPrefs.GetInt("VolumeInfo");
        }
    }
}
