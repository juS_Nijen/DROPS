﻿using UnityEngine;

/// <summary>
/// 처음 시작 시, 화면을 초기화 해 주는 역할.
/// 수정 할 필요가 없습니다.
/// </summary>
public class ScreenSetting : MonoBehaviour {

    private void Start()
    {
        Time.timeScale = 1;
        Screen.SetResolution(Screen.width, Screen.width / 9 * 16, true);
    }
}