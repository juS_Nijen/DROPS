﻿using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// 저장, 종료, 일시정지와 같은 컨트롤을 관리한다.
/// </summary>
public class GameStates : MonoBehaviour
{
    // 이 스크립트를 넣을 때는 게임 메니저를 반드시 찾아 넣어주도록 한다.
    public GameObject gameManager;
    public static GameStates gameStates;

    private bool uiPause = false;
    private Timer timerScript;

    private int checkNum;
    public void setCheckNum(int _checkNum) { checkNum = _checkNum; }


    private void Start()
    {
        GmScript.gameManagerScript.CatchNullExeption(gameManager);
        timerScript = gameManager.GetComponent<Timer>();
    }


    /// <summary>
    /// Time.timeScale를 이용하여 유니티를 일시정지 시킨다.
    /// </summary>
    public void GameS_Pause() {
        Time.timeScale = 0.0f;
        if (checkNum == 1) PauseTimer();
    }
    /// <summary>
    /// Time.timeScale를 이용하여 유니티를 일시정지 시킨다.
    /// </summary>
    public void GameS_Resume() {
        if (uiPause) return;
        Time.timeScale = 1.0f;
    }

    /// <summary>
    /// 입력받은 sceneName로 씬을 전환한다.
    /// </summary>
    /// <param name="sceneName"></param>
    public void SceneChange(string sceneName) {
        데이터저장_임시();

        Debug.Log("##" + sceneName + "Scene##");
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// QUIT 버튼을 눌렀을 시, 종료된다.
    /// </summary>
    public void GameS_QUIT() {
        //if (Input.GetKey("escape")) //뒤로가기버튼
        //확인하는 팝업을 띄울까?
        데이터저장_임시();
        Application.Quit();
    }


    public void 데이터저장_임시() {
        //저장하기전에 종료되면 안됨
    }

    /// <summary>
    /// OnUIPuase는 UI에서 pause가 필요할때 true로 설정합니다.
    /// </summary>
    public void SetOnUIPuase(bool ui)
    { uiPause = ui; }

    //▼ 재호야ㅑ아ㅏ아아아아아악
    /// <summary>
    /// Timer가 계속 돌아가도록 하려고 했는데, 에러가 나는 듯 하다.
    /// </summary>
    public void PauseTimer()
    {
        timerScript.StartCoroutine(timerScript.CoroutineTimer(0.1f, 0));
    }
}