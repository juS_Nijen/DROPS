﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 점수, 오브젝트의 생성 등 오브젝트와 관련 된 기능들이 들어있습니다.
/// Public 함수 : SetScore // SetScore // Pang_Upper3 // PrefabObject
/// </summary>
public class ObjectControl : MonoBehaviour {

    private PrefabSpwan psScript;

    //점수에 곱해지는 값
    private int scoreMultyfy = 122;
    private int score = 0;

    //UI의 Score를 설정에 사용된다.
    private Text ScoreBox;

    private void Start()
    {
        ScoreBox = GameObject.Find("Scoretext").GetComponent<Text>();
    }


    //Scroe의 Getter, Setter
    public void SetScore(int _PlusScore) {
        score = _PlusScore;
        ScoreBox.text = score.ToString();
    }
    public int GetScore() { return score; }


    //제목이 구리다. 작성자 본인도 알고있는 문제이기에
    //마음에 안들면 알아서 수정해도 좋다.


    /// <summary>
    /// 5개 이상의 오브젝트를 터트려주는 메서드.
    /// </summary>
    public void Pang_Upper3()
    {   Debug.Log("ObjectControl-Pang_Upper3");
        int PlusScore;

        PlusScore = scoreMultyfy * GmScript.gameManagerScript.GetCountObject();
        SetScore(GetScore() + PlusScore);
    }


    /// <summary>
    /// 5개 이상의 오브젝트를 터트려주는 메서드.
    /// </summary>
    public void Pang_Upper5()
    {   Debug.Log("ObjectControl-Pang_Upper5");
        int PlusScore;

        PlusScore = (scoreMultyfy * GmScript.gameManagerScript.GetCountObject() * 3);
        SetScore(GetScore() + PlusScore);
    }
}
